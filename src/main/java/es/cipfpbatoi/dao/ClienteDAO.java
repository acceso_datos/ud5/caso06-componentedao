package es.cipfpbatoi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import es.cipfpbatoi.modelo.Cliente;

/**
 *
 * @author sergio
 */
public class ClienteDAO implements GenericDAO<Cliente> {
	final String TABLA = "cliente";
	final String PK = "id";

	final String SQLSELECTALL = "SELECT * FROM " + TABLA;
	final String SQLSELECTCOUNT = "SELECT count(*) FROM " + TABLA;
	final String SQLSELECTPK = "SELECT * FROM " + TABLA + " WHERE " + PK + " = ?";
	final String SQLINSERT = "INSERT INTO " + TABLA + " (nombre, direccion) VALUES (?, ?)";
	final String SQLUPDATE = "UPDATE " + TABLA + " SET nombre = ?, direccion = ? WHERE " + PK + " = ?";
	final String SQLDELETE = "DELETE FROM " + TABLA + " WHERE " + PK + " = ?";
	private final PreparedStatement pstSelectPK;
	private final PreparedStatement pstSelectAll;
	private final PreparedStatement pstSelectCount;
	private final PreparedStatement pstInsert;
	private final PreparedStatement pstUpdate;
	private final PreparedStatement pstDelete;

	public ClienteDAO() throws SQLException {
		Connection con = ConexionBD.getConnection();
		pstSelectPK = con.prepareStatement(SQLSELECTPK);
		pstSelectCount = con.prepareStatement(SQLSELECTCOUNT);
		pstSelectAll = con.prepareStatement(SQLSELECTALL);
		pstInsert = con.prepareStatement(SQLINSERT, PreparedStatement.RETURN_GENERATED_KEYS);
		pstUpdate = con.prepareStatement(SQLUPDATE);
		pstDelete = con.prepareStatement(SQLDELETE);
	}

	public void close() throws SQLException {
		pstSelectPK.close();
		pstSelectAll.close();
		pstSelectCount.close();
		pstInsert.close();
		pstUpdate.close();
		pstDelete.close();
	}

	private Cliente build(ResultSet rs) throws SQLException {
		return new Cliente(rs.getInt("id"), rs.getString("nombre"), rs.getString("direccion"));
	}

	private void setPreparedInsert(Cliente registro) throws SQLException {
		pstInsert.setString(1, registro.getNombre());
		pstInsert.setString(2, registro.getDireccion());
	}

	private void setGeneratedKey(Cliente registro) throws SQLException {
		ResultSet rsClave = pstInsert.getGeneratedKeys();
		rsClave.next();
		int idAsignada = rsClave.getInt(1);
		registro.setId(idAsignada);
		rsClave.close();
	}

	private void setPreparedUpdate(Cliente registro) throws SQLException {
		pstUpdate.setString(1, registro.getNombre());
		pstUpdate.setString(2, registro.getDireccion());
		pstUpdate.setInt(3, registro.getId());
	}


	public Cliente find(int id) throws SQLException {
		Cliente c = null;
		pstSelectPK.setInt(1, id);
		ResultSet rs = pstSelectPK.executeQuery();
		if (rs.next()) {
			c = build(rs);
		}
		rs.close();
		return c;

	}

	public List<Cliente> findAll() throws SQLException {
		List<Cliente> listaClientes = new ArrayList<Cliente>();
		ResultSet rs;
		rs = pstSelectAll.executeQuery();
		while (rs.next()) {
			listaClientes.add(build(rs));
		}
		rs.close();
		return listaClientes;

	}

	public Cliente insert(Cliente cliInsertar) throws SQLException {
		setPreparedInsert(cliInsertar);
		int insertados = pstInsert.executeUpdate();
		if (insertados == 1) {
			setGeneratedKey(cliInsertar);
			return cliInsertar;
		}
		return null;
	}

	public boolean update(Cliente cliActualizar) throws SQLException {
		setPreparedUpdate(cliActualizar);
		int actualizados = pstUpdate.executeUpdate();
		return (actualizados == 1);
	}

	public boolean save(Cliente cliGuardar) throws SQLException {
		if (this.exists(cliGuardar.getId())) {
			return this.update(cliGuardar);
		} else {
			return (this.insert(cliGuardar) != null);
		}
	}

	public boolean delete(int id) throws SQLException {
		pstDelete.setInt(1, id);
		int borrados = pstDelete.executeUpdate();
		return (borrados == 1);
	}

	public boolean delete(Cliente cliEliminar) throws SQLException {
		return this.delete(cliEliminar.getId());
	}

	public long size() throws SQLException {
		ResultSet rs;
		rs = pstSelectCount.executeQuery();
		rs.next();
		return rs.getInt(1);
	}

	public boolean exists(int id) throws SQLException {
		if (find(id) != null) {
			return true;
		}
		return false;
	}	

}
